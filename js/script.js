const accordionItems = document.querySelectorAll(".accordion__item");
// accordion
accordionItems.forEach((item) =>
  item.addEventListener("click", function (e) {
    if (
      Array.from(this.childNodes[3].classList).includes(
        "accordion__text-hidden"
      )
    ) {
      this.childNodes[3].classList.remove("accordion__text-hidden");
    } else {
      this.childNodes[3].classList.add("accordion__text-hidden");
    }
  })
);
// Телефон
// Маска для телефона
let inp = document.querySelector(".form__tel");

inp.addEventListener("input", function () {});
// Отменяем ввод не цифр
inp.addEventListener("keypress", (e) => {
  if (!/\d/.test(e.key)) e.preventDefault();
});
let old = 0;
const numberArr = ["1", "2", "3", "4", "5", "6"];
if (inp.value[0] == "9" || inp.value[0] == "7" || inp.value[0] == "8") {
}
inp.addEventListener("input", function () {
  if (inp.value[0] == "7") {
    inp.value = "+" + inp.value;
  } else if (inp.value[0] == "9") {
    inp.value = "+7 " + inp.value;
  } else if (inp.value[0] == "8") {
    inp.value = "";
    inp.value = " 8" + inp.value;
  }
  // Проверка еди первое число не 7,8,9 то не будет ввода
  numberArr.forEach((num) => {
    if (inp.value[0] == num) {
      inp.value = "";
    }
  });
  let inpValueLength = inp.value.length;
  if (inpValueLength < old) {
    old--;
    return;
  }

  if (
    inpValueLength == 2 ||
    inpValueLength == 6 ||
    inpValueLength == 10 ||
    inpValueLength == 13
  )
    inp.value = inp.value + " ";
  if (inpValueLength > 16)
    inp.value = inp.value.substring(0, inp.value.length - 1);

  old++;
});
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Форма
const input = document.querySelectorAll(".form__input");
// Подключаюсь к полю майл
let domMail;

(domMail = document.querySelector("#email"))
// SmtpJs
function SendMail() {
  // Проверка емейла
  const arr = [];
  if (
    input[2].value.endsWith("@gmail.com") === true ||
    input[2].value.endsWith("@yandex.ru") === true ||
    input[2].value.endsWith("@yaho.com") === true ||
    input[2].value.endsWith("@mail.ru") === true
  ) {
    arr.push(true);
  } else {
    alert("Введите емейл");
  }
  // Проверка телефона
  if (input[1].value.length === 16) {
    arr.push(true);
  } else {
    alert("Введите номер телефона");
  }
  // Проверка имени
  if (input[0].value != "") {
    arr.push(true);
  } else {
    alert("Заполните имя");
  }

  if (arr.length === 3) {
    const arrInp = [];
    input.forEach((inp) => {
      arrInp.push(inp.value);
    });

    Email.send({
      Host: "smtp.elasticemail.com",
      Username: "demigot2033@gmail.com",
      Password: "616F16B2DAF80C9C7A3CDCACD0272998C1C2",
      // На эту почту приходят письма
      // Пишем свою почту сейчас тут поле почты
      To: `${domMail.value}`,
      // !!!!!!!!!!!!!!!!
      From: "demigot2033@gmail.com",
      Subject: "Это письмо с формы",
      Body: `<ul">
      <li>Имя: ${arrInp[0]}</li>
      <li>Телефон: ${arrInp[1]}</li>
      <li>Mail: ${arrInp[2]}</li>
      </ul>`,
    }).then((message) => alert(message));
    document
      .querySelectorAll(".form__input")
      .forEach((dom) => (dom.value = ""));
  }
}

domMail.parentNode.addEventListener("mouseover", function () {
  document.querySelector(".form__inner-mail").classList.remove("d-none");
});
domMail.parentNode.addEventListener("mouseout", function () {
  document.querySelector(".form__inner-mail").classList.add("d-none");
});
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Диаграммы

class DiagramAdd {
  myChart;
  constructor(
    id,
    unClass,
    countriesSize,
    countries,
    bgColor,
    cartText,
    edenis
  ) {
    this.id = id;
    this.unClass = unClass;
    this.countriesSize = countriesSize;
    this.countries = countries;
    this.bgColor = bgColor;
    this.cartText = cartText;
    this.edenis = edenis;
  }
  main() {
    document.querySelector(".diagram__wrapper").insertAdjacentHTML(
      "beforeend",
      `
    <div class="diagram__cart ${this.unClass}">
    <canvas class="chart" id="${this.id}"></canvas>
  </div>
    `
    );
    const ctx = document.getElementById(this.id);
    this.myChart = new Chart(ctx, {
      type: "doughnut",
      data: {
        labels: [...this.countries],
        datasets: [
          {
            // label: '# of Votes',
            data: [...this.countriesSize],
            hoverOffset: 10,
            cutout: "80%",
            radius: "85%",
            // поворот
            rotation: 270,
            hoverBorderWidth: 5,
            hoverBorderJoinStyle: "miter",
            // hoverBorderDashOffset:,
            // hoverBorderDash:,
            backgroundColor: [...this.bgColor],
            hoverBorderColor: [...this.bgColor],
            hoverBackgroundColor: [...this.bgColor],
            borderRadius: -20,
          },
        ],
      },
      options: {
        onHover: (event, element) => {
          if (element.length === 1) {
            const span = document.querySelectorAll(".legend__span");

            let dugaBg = element[0].element.options.backgroundColor;
            // Обрезка rgb
            dugaBg = dugaBg.replaceAll("rgb", "");
            dugaBg = dugaBg.replaceAll("(", "");
            dugaBg = dugaBg.replaceAll(")", "");
            dugaBg = dugaBg.replaceAll(" ", "");
            // Окраска списка
            span.forEach((item) => {
              let spanBg = item.style.background;
              // Обрезка rgb
              spanBg = spanBg.replaceAll("rgb", "");
              spanBg = spanBg.replaceAll("(", "");
              spanBg = spanBg.replaceAll(")", "");
              spanBg = spanBg.replaceAll(" ", "");

              if (dugaBg === spanBg) {
                item.parentNode.style = "color:#626262";
                item.style.opacity = 1;
              } else {
                item.style.opacity = 0.5;
                item.parentNode.style = "color:#A7A7A7";
              }
            });
          }
        },
        plugins: {
          legend: {
            maxWidth: 1,
            position: "bottom",
            // display: false,
            // maxWidth:20,
            align: "start",
            display: false,
            labels: {
              padding: 20,
              maxHeight: 13,
              boxWidth: 23,
              usePointStyle: 20,
              fontColor: "black",
              fontFamily: "Calibri Light",
              fontStyle: "italic",
            },
          },
        },
      },
    });
    this.generationLegend();
    this.getSum(this.edenis + ".", this.countriesSize);
    this.getText(this.cartText);
  }
  // Список
  generationLegend() {
    const ChartBox = document.querySelector("." + this.unClass);
    // создаю div
    const div = document.createElement("div");
    div.setAttribute("id", "customLegend");
    // Создаю список
    const ul = document.createElement("ul");
    // вставка div
    ChartBox.appendChild(div);
    div.appendChild(ul);
    this.myChart.legend.legendItems.forEach((legend) => {
      ul.insertAdjacentHTML(
        "beforeend",
        `<li class = "legend__list"><span class = "legend__span" style = "background:${legend.fillStyle}">1</span>${legend.text}</li>`
      );
    });
    if (ChartBox.childNodes[3].childNodes[0].childNodes.length > 3) {
      ChartBox.childNodes[3].childNodes[0].classList.add("legend-grid");
    }
  }
  // Число внутри диаграммы
  getSum(ed, sum) {
    const cart = document.querySelector("." + this.unClass);

    sum = sum.reduce((acc, num) => acc + num);
    if (ed == "млн.") {
      cart.insertAdjacentHTML(
        "beforeend",
        `
    <div class = "diagram__num cart-decore"><h4>$${sum}</h4><h6>${ed}</h6></div>  
  `
      );
    } else {
      cart.insertAdjacentHTML(
        "beforeend",
        `
    <div class = "diagram__num"><h4>${sum}</h4><h6>${ed}</h6></div>  
  `
      );
    }
  }
  getText(text) {
    const cart = document.querySelector("." + this.unClass);
    cart.insertAdjacentHTML(
      "beforeend",
      `
    <div class = "diagram__naming">${text}<div>
    `
    );
  }
}
const one = new DiagramAdd(
  "myChart",
  "cart-one",
  [167, 120, 48],
  ["- Россия", "- Казахстан", "- Узбекистан"],
  ["rgb(232,60,70)", "rgb(249, 166, 32)", "rgb(71, 68, 68)"],
  "Страны",
  "шт"
);
one.main();
const two = new DiagramAdd(
  "myChartq",
  "cart-two",
  [190, 340, 167, 221, 147],
  ["- Тюмень", "- Петербург", "- Нур-Султан", "- Алма-Аты", "- Ташкент"],
  [
    "rgb(209, 47, 57)",
    "rgb(0, 95, 167)",
    "rgb(230, 155, 33)",
    "rgb(121, 121, 121)",
    "rgb(56, 53, 53)",
  ],
  "Города",
  "шт"
);
two.main();
const tree = new DiagramAdd(
  "myChartqe",
  "cart-tree",
  [65, 45, 20],
  ["- Россия", "- Казахстан", "- Узбекистан"],
  ["rgb(175, 28, 37)", "rgb(248, 166, 32)", "rgb(72, 68, 68)"],
  "Страны",
  "млн"
);
tree.main();
const four = new DiagramAdd(
  "myChartqq",
  "cart-four",
  [22, 43, 21, 23, 21],
  ["- Тюмень", "- Петербург", "- Нур-Султан", "- Алма-Аты", "- Ташкент"],
  [
    "rgb(209, 47, 58)",
    "rgb(0, 94, 167)",
    "rgb(230, 153, 33)",
    "rgb(121, 122, 121)",
    "rgb(56, 52, 53)",
  ],
  "Города",
  "млн"
);
four.main();
// Кнопка над диаграмами
document.querySelector(".diagram__btn").addEventListener("click", function () {
  if (this.childNodes[0].classList.length === 2) {
    this.childNodes[0].classList.remove("ml-auto");
    document
      .querySelectorAll(".diagram__text")[1]
      .classList.add("decore__text-decore");
    document
      .querySelectorAll(".diagram__text")[0]
      .classList.remove("decore__text-decore");
  } else {
    document
      .querySelectorAll(".diagram__text")[1]
      .classList.remove("decore__text-decore");
    document
      .querySelectorAll(".diagram__text")[0]
      .classList.add("decore__text-decore");
    this.childNodes[0].classList.add("ml-auto");
  }
});
